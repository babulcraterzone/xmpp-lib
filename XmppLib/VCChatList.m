//
//  VCChatList.m
//  XmppLib
//
//  Created by Babul Prabhakar on 03/05/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import "VCChatList.h"
#import "Constants.h"
#import "ViewController.h"
#import "PrototypeCell.h"
#import "MessageController.h"
#import "XMPPController.h"
#import "Chat.h"
@interface VCChatList ()<UITableViewDataSource,UITableViewDelegate> {
    NSMutableArray *_chatList ;
}

@end

@implementation VCChatList

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.tableView reloadData];
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addRosterClicked:)];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateRoster:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateRoster:) name:UPDATE_CHAT_LIST object:nil];
    
}


-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UPDATE_CHAT_LIST object:nil];
    
}


-(void)updateRoster:(NSNotification *)notif {
    
    _chatList = [NSMutableArray array];
    MessageController *messageController = [[XMPPController sharedSingleton]messageController];
    [_chatList addObjectsFromArray:[messageController getAllChatList]];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addRosterClicked:(id)sender {
    
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Enter Jid and Table" message:@"JID must not contain domain server name" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [av setAlertViewStyle:UIAlertViewStylePlainTextInput];
    av.delegate = self;
    // Alert style customization
    [[av textFieldAtIndex:0] setPlaceholder:@"User Jid"];
        [av show];
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
   
    if (buttonIndex ==1) {
        
    
    NSString *jid = [alertView textFieldAtIndex:0].text;
      MessageController *messageController = [[XMPPController sharedSingleton]messageController];
    [messageController createChatForJID:jid withDisplayName:@"User"];

    [self.tableView reloadData];
    }
    
}

#pragma mark - Table View Delegates -
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _chatList.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    

    ViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatView"];
  
    vc.currentChat = _chatList[indexPath.row];
    [self.navigationController pushViewController:vc
                                         animated:YES];

    
    
}


-(PrototypeCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PrototypeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
    Chat *chat = _chatList[indexPath.row];
    cell.titleLbl.text = chat.jid;
 
    return cell;
    
}



@end
