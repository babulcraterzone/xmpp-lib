//
//  XMPPMessage+CustomProperty.h
//  XmppLib
//
//  Created by Babul Prabhakar on 01/05/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import "XMPPMessage.h"

@interface XMPPMessage (CustomProperty)
-(void)addPropertyWithName:(NSString *)name andValue:(NSString *)value  andType:(NSString *)type;
-(void)addNumberPropertyWithName:(NSString *)name andValue:(NSNumber *)value  andType:(NSString *)type;
@end
