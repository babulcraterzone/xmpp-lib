//
//  FileResponse.h
//  ChatNa
//
//  Created by Babul Prabhakar on 31/07/14.
//  Copyright (c) 2014 Babul Prabhakar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileUploadResponse : NSObject
@property (nonatomic,strong)NSString *contentId;

-(id)initWithDict:(NSDictionary *)dict;
@end
