//
//  Message.h
//  XmppLib
//
//  Created by Babul Prabhakar on 18/04/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//
//Subclass MEssage Class  To Create Custom Message
#import <Foundation/Foundation.h>
#import "Constants.h"
@class XMPPMessage;
@class XMPPStream;
@class XMPPMessageArchivingCoreDataStorage;

@interface Message : NSObject
@property (nonatomic)MessageType msgType;
@property (nonatomic,strong)NSString *messageId;
@property (nonatomic,strong)NSString *msgJid;
@property (nonatomic)BOOL isOutgoing;
@property (nonatomic,strong)NSDate *messageDateAndTime;
@property (nonatomic)MessageStatus messageStatus;
-(id)initWithJid:(NSString *)jid AndMsgId:(NSString *)msgId;

-(XMPPMessage *)parseToXMPPMessage;

-(id)initWithXmppMessage:(XMPPMessage *)xmppMessage;

-(NSString *)toString;




+(NSArray *)arrayOfMessageFromXMPPMessageArchivingCoreDataStorage:(NSArray *)messageArray;
@end
