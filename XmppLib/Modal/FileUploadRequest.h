//
//  FileUploadRequest.h
//  XmppLib
//
//  Created by Babul Prabhakar on 01/05/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileUploadRequest : NSObject

@property (nonatomic,strong)NSString * exten;
@property (nonatomic,strong)NSData * data;
@property (nonatomic,strong)NSString *type;
@property (nonatomic,strong) NSString *fileName;

@end
