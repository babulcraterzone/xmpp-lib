//
//  Chat.h
//  XmppLib
//
//  Created by Babul Prabhakar on 18/04/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
@class Message;
@class XMPPMessageArchiving_Contact_CoreDataObject;
@protocol ChatMessageDelegate <NSObject>
@optional
-(void) handleMessageSent:(Message*) aMessage;


/*!
 *@author Babul Prabhakar
 * @discussion Delegate method gets called when the message status Changes example message has been delivred or read by user
 * @param aBareJid,messageId,MessageStatus
 * @return void
 */
-(void) handleMessageChange:(NSString*) aBareJid messageId:(NSString *)messageId status:(MessageStatus)newState;

@end


@interface Chat : NSObject {
    int _lastMsgNumber;
}

-(instancetype)initWithChatJID:(NSString *)jid WithName:(NSString *)name;
@property (nonatomic,strong,setter=setChatDisplayName:)NSString *chatDisplayName;
@property (nonatomic,strong,readonly)NSString *jid;
@property (nonatomic,strong,getter=chatID)NSString *chatId;
@property (nonatomic,strong)NSString *mostRecentMsgStr;
@property (nonatomic)BOOL isMostRecentMsgOutgoing;
@property (nonatomic,strong)NSDate *mostRecentMessageTimestamp;
@property (nonatomic,weak)id<ChatMessageDelegate>chatDelegate;


-(void)handleRecievedMessage:(Message *)message;
-(void)sendMessage:(Message *)message;
-(NSArray *)loadMessagesWithCurrentOffset:(int)currentOffset andLimit:(int)limit;
+(NSDictionary *)arrayOfChatFromXMPPMessageArchiving_Contact_CoreDataObject:(NSArray *)contactArr;

@end
