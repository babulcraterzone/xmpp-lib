//
//  TextMessage.h
//  XmppLib
//
//  Created by Babul Prabhakar on 18/04/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import "Message.h"

@interface TextMessage : Message
@property (nonatomic,strong)NSString *body;

-(id)init;
-(id)initWithXmppMessage:(XMPPMessage *)xmppMessage;
-(NSString *)toString;



@end
