//
//  FileResponse.m
//  ChatNa
//
//  Created by Babul Prabhakar on 31/07/14.
//  Copyright (c) 2014 Babul Prabhakar. All rights reserved.
//

#import "FileUploadResponse.h"

@implementation FileUploadResponse

-(id)initWithDict:(NSDictionary *)dict {
    
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
    }
    
    return self;
}
@end
