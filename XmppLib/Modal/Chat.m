//
//  Chat.m
//  XmppLib
//
//  Created by Babul Prabhakar on 18/04/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import "Chat.h"
#import "MessageController.h"
#import "XMPPController.h"
#import "Message.h"
#import "Constants.h"
#import "XMPPStream.h"
#import "XmppStorageManager.h"
#import "TextMessage.h"
#import "FileMessage.h"
#import "XMPPMessageArchivingCoreDataStorage.h"
#import "XMPPMessageArchiving_Message_CoreDataObject.h"
@implementation Chat
@synthesize jid = _jid;
@synthesize chatDisplayName = _chatDisplayName;
-(instancetype)initWithChatJID:(NSString *)jid WithName:(NSString *)name {
    
    if (self = [self init]) {
        _jid = [Constants bareJID:jid];
        _chatDisplayName = name;
        _lastMsgNumber = 0;
    }
    return self;
}

-(id) init
{
    if(self = [super init]) {
       
    }
   
    return self;
}




-(void)setChatDisplayName:(NSString *)chatDisplayName {
    
    _chatDisplayName = chatDisplayName;
    
}

-(void)handleRecievedMessage:(Message *)message {
    
    if (message)
    {
        message.isOutgoing = NO;
        [self doHandleReceivedMessage:message];
        
    }
}

-(void)sendMessage:(Message *)message {
    
    
    if( !message )
        return;
    message.messageId = [NSString stringWithFormat:@"%@-%d",self.chatId,(++_lastMsgNumber)];
    message.msgJid = self.jid;
    
    message.messageStatus = MESSAGE_STATUS_WAITING;
    [self updateLastChat:message];
    [self saveChatMessage:message];
    [[[XMPPController sharedSingleton]  messageController] sendOrQueueChatMessage:message];
    
}

-(NSArray *)loadMessagesWithCurrentOffset:(int)currentOffset andLimit:(int)limit{
    return  [[XmppStorageManager sharedSingleton] loadMessagesForJid:self.jid withLimit:limit withOffset:currentOffset];
    
}


-(void)updateLastChat:(Message *)message {
    self.mostRecentMessageTimestamp = message.messageDateAndTime;
    self.isMostRecentMsgOutgoing = message.isOutgoing;
    self.mostRecentMsgStr = message.toString;
}

-(void) saveChatMessage:(Message*)message
{
    [[XmppStorageManager sharedSingleton]saveOrUpdateMessage:message];
    
}



-(NSString*) getDisplayName
{
    if( _chatDisplayName )
        return _chatDisplayName;
    
    return @"";
}

-(void)sendLocalNotifWithDisplayName:(NSString *)displayName Body: (NSString *)body {
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
   
    //check from data controller that user has defined the messageController yes or not;
    NSString* bodyMsg = [NSString stringWithFormat:@"%@: %@",displayName,body];
    
    localNotification.applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
    localNotification.alertBody = bodyMsg;
   
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
}

-(void) doHandleReceivedMessage:(Message*) aMessage
{
    NSString* displayName = [self getDisplayName];
    
    
    if (![[UIApplication sharedApplication] applicationState] == UIApplicationStateActive )
    {
        // We are not active, so use a local notification instead
        [self sendLocalNotifWithDisplayName:displayName Body:aMessage.toString];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:MESSAGE_RECIEVED_NOTIFICATION object:aMessage   userInfo:nil];
    }
    
}


+(NSDictionary *)arrayOfChatFromXMPPMessageArchiving_Contact_CoreDataObject:(NSArray *)contactArr {
    
    NSMutableDictionary *chatList = [NSMutableDictionary dictionary];
    for (XMPPMessageArchiving_Contact_CoreDataObject *contact in contactArr)
    {
        Chat *chatObj = [[Chat alloc] initWithChatJID:contact.bareJidStr WithName:contact.bareJidStr];
        chatObj.mostRecentMessageTimestamp = contact.mostRecentMessageTimestamp;
        chatObj.isMostRecentMsgOutgoing = [contact.mostRecentMessageOutgoing boolValue];
        chatObj.mostRecentMsgStr = contact.mostRecentMessageBody;
        [chatList setObject:chatObj forKey:chatObj.jid];
    }
    
    return chatList;
}

-(NSString *)chatID {
    
    if (_chatId == nil) {
        _chatId = [XMPPController sharedSingleton].xmppStream.generateUUID;
    }
    
    return _chatId;
}
@end
