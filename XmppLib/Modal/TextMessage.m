//
//  TextMessage.m
//  XmppLib
//
//  Created by Babul Prabhakar on 18/04/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import "TextMessage.h"
#import "XMPPMessage.h"
#import "Constants.h"
#import "NSXMLElement+XMPP.h"
@implementation TextMessage


-(id)initWithXmppMessage:(XMPPMessage *)xmppMessage {
    if (self = [super initWithXmppMessage:xmppMessage]) {
    //find body and put in msg body
        if( ![xmppMessage isErrorMessage] )
        {
            // message
            self.body = xmppMessage.body;
           
            
        }

    }
    return self;
}

-(id)init {
    
    if (self = [super init]) {
        self.msgType = TEXT_TYPE_MESSAGE;
        
    }
    
    return self;
}

-(XMPPMessage *)parseToXMPPMessage {
    
    XMPPMessage *aXmppMessage = [super parseToXMPPMessage];
    
    NSXMLElement * body = [NSXMLElement elementWithName:TAG_BODY stringValue:self.body] ;
    [aXmppMessage addChild:body];
    
    return aXmppMessage;
    
    
}

-(NSString *)toString {
    
    return self.body;
    
}
@end
