//
//  Message.m
//  XmppLib
//
//  Created by Babul Prabhakar on 18/04/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import "Message.h"
#import "XMPPMessage.h"

#import "Constants.h"
#import "NSXMLElement+XMPP.h"
#import "DDXML.h"
#import "XMPPMessageArchiving_Message_CoreDataObject.h"
#import "FileMessage.h"
#import "TextMessage.h"
#import "XMPPMessage+CustomProperty.h"
#import <objc/runtime.h>
@implementation Message

-(id)initWithJid:(NSString *)jid AndMsgId:(NSString *)msgId; {
    
    if (self = [super init]) {
        self.isOutgoing = YES;
        self.messageId = msgId;
         self.messageDateAndTime = [NSDate date];
        
    }
    return self;
}


-(id)init {
    if (self = [super init]) {
        self.isOutgoing = YES;
        self.messageDateAndTime = [NSDate date];
        
    }
    return self;
    
}

-(XMPPMessage *)parseToXMPPMessage {
    NSXMLElement *message = [NSXMLElement elementWithName:TAG_MESSAGE];
    
    [message addAttributeWithName:@"id" stringValue:self.messageId];
    
    [message addAttributeWithName:@"to" stringValue:self.msgJid];
    
    [message addAttributeWithName:TAG_TYPE stringValue:@"chat"];
    
    NSXMLElement *properties = [NSXMLElement  elementWithName:KTAG_PROPERTIES];
    [properties addAttributeWithName:@"xmlns" stringValue:@"http://www.jivesoftware.com/xmlns/xmpp/properties"];
    [message addChild:properties];
    
    XMPPMessage * xMessage = [XMPPMessage messageFromElement:message];
    [xMessage addPropertyWithName:MESSAGE_TYPE andValue:[Constants messageTypeStr:self.msgType] andType:KTAG_STRING];

    
    return xMessage;
    
}
-(id)initWithXmppMessage:(XMPPMessage *)xmppMessage {
    
    
    if (self = [super init])
    {
        if( ![xmppMessage isErrorMessage] )
        {
            // message
            XMPPJID* xmppjid = xmppMessage.from;
            self.msgJid = xmppjid.bare;
            
            self.messageId = [xmppMessage attributeStringValueForName:@"id"];
            self.isOutgoing = NO;
            self.messageDateAndTime = [NSDate date];
            
            
            NSXMLElement *properties = [xmppMessage elementForName:KTAG_PROPERTIES];
            if (properties)
            {
                NSArray *propertys = [properties elementsForName:KTAG_PROPERTY];
                for (int i=0; i<[propertys count]; i++)
                {
                    NSXMLElement *property = [propertys objectAtIndex:i];
                    
                    NSString *name = [[property elementForName:@"name"] stringValue];
                    NSString *value = [[property elementForName:@"value"] stringValue];
                    
                    if( [name isEqualToString:MESSAGE_TYPE] )
                    {
                        self.msgType = [Constants messageTypeFromStr:value];
                    }
                }
            }
        }
        
        
    }
    return self;
    
}


-(NSString *)toString {
    
    [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"Subclass Method should be called not this one"] userInfo:nil];
    return nil;
}






+(NSArray *)arrayOfMessageFromXMPPMessageArchivingCoreDataStorage:(NSArray *)messageArray {
    _START
    NSMutableArray *returnArr = [NSMutableArray array];
    for (XMPPMessageArchiving_Message_CoreDataObject *msg in messageArray) {
        Message *message = nil;
        
        if (msg.messageType == TEXT_TYPE_MESSAGE) {
            TextMessage *txtMessage = [[TextMessage alloc] initWithXmppMessage:msg.message];
            txtMessage.isOutgoing = msg.isOutgoing;
            txtMessage.messageDateAndTime = msg.timestamp;
            message = txtMessage;
            
        }
        
        if ((msg.messageType == IMAGE_TYPE_MESSAGE)|| (msg.messageType == AUDIO_TYPE_MESSAGE)||(msg.messageType == VIDEO_TYPE_MESSAGE)){
            FileMessage *fileMessage = [[FileMessage alloc] initWithXmppMessage:msg.message];
            fileMessage.isOutgoing = msg.isOutgoing;
            fileMessage.messageDateAndTime = msg.timestamp;
            fileMessage.fileURI = msg.body;
            message = fileMessage;
            
        }
        XMPPJID *jid = (msg.isOutgoing) ? msg.message.to : msg.message.from;
        message.msgJid = jid.bare;
        [returnArr addObject:message];
        
    }
    
    _END
      return returnArr;
    
}



@end
