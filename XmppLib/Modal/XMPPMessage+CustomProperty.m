//
//  XMPPMessage+CustomProperty.m
//  XmppLib
//
//  Created by Babul Prabhakar on 01/05/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import "XMPPMessage+CustomProperty.h"
#import "Constants.h"
#import "NSXMLElement+XMPP.h"
@implementation XMPPMessage (CustomProperty)
-(void)addPropertyWithName:(NSString *)name andValue:(NSString *)value  andType:(NSString *)type{
    NSXMLElement *propertyList = [self elementForName:KTAG_PROPERTIES];
   
    //add name
    NSXMLElement *property = [NSXMLElement  elementWithName:KTAG_PROPERTY];
    NSXMLElement *propertyName = [NSXMLElement  elementWithName:KTAG_NAME];
    [propertyName setStringValue:name];
    [property addChild:propertyName];
    //add value
    NSXMLElement *propertyValue = [NSXMLElement  elementWithName:KTAG_VALUE];
    [propertyValue addAttributeWithName:@"type" stringValue:type];
    [propertyValue setStringValue:value];
    [property addChild:propertyValue];
    
   
    //add property to property list
    [propertyList addChild:property];

    
    
}



-(void)addNumberPropertyWithName:(NSString *)name andValue:(NSNumber *)value  andType:(NSString *)type{
    NSXMLElement *propertyList = [self elementForName:KTAG_PROPERTIES];
    
    //add name
    NSXMLElement *property = [NSXMLElement  elementWithName:KTAG_PROPERTY];
    
    NSXMLElement *propertyName = [NSXMLElement  elementWithName:KTAG_NAME stringValue:name];
    [property addChild:propertyName];
    //add value
     NSXMLElement *propertyValue  = [NSXMLElement  elementWithName:KTAG_VALUE numberValue:value];
     [propertyValue addAttributeWithName:KTAG_TYPE stringValue:type];
    [property addChild:propertyValue];
    
    
    //add property to property list
    [propertyList addChild:property];
    
    
    
}




@end
