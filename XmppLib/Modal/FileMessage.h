//
//  FileMessage.h
//  XmppLib
//
//  Created by Babul Prabhakar on 30/04/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import "Message.h"

@interface FileMessage : Message
@property (nonatomic,strong)NSString *hresUrl;
@property (nonatomic,strong)NSString *lresUrl;
@property(nonatomic,strong)NSString *fileURI;
-(id)init;
-(id)initWithXmppMessage:(XMPPMessage *)xmppMessage;
-(NSString *)toString;
@end
