//
//  FileMessage.m
//  XmppLib
//
//  Created by Babul Prabhakar on 30/04/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import "FileMessage.h"
#import "XMPPMessage.h"
#import "NSXMLElement+XMPP.h"
#import "DDXML.h"
#import "XMPPMessage+CustomProperty.h"
@implementation FileMessage
-(id)initWithXmppMessage:(XMPPMessage *)xmppMessage {
    if (self = [super initWithXmppMessage:xmppMessage]) {
    
        //find body and put in msg body
        if( ![xmppMessage isErrorMessage] )
        {
            // message
            NSXMLElement *properties = [xmppMessage elementForName:KTAG_PROPERTIES];
            if (properties)
            {
                NSArray *propertyArray = [properties elementsForName:KTAG_PROPERTY];
                
                 for ( NSXMLElement *property in propertyArray)  {
                   
                    NSString *name = [[property elementForName:@"name"] stringValue];
                    NSString *value = [[property elementForName:@"value"] stringValue];
                    if( [name isEqualToString:HRES_URL]  )
                    {
                        self.hresUrl = value;
                    }
                    else if( [name isEqualToString:LRES_URL]  )
                    {
                        self.lresUrl = value;
                    }
                    
                    
                }
                
            }
            
        }
    }
    return self;
}

-(id)init {
    
    if (self = [super init]) {
        self.msgType = IMAGE_TYPE_MESSAGE;
        
    }
    
    return self;
}
-(id)initWithType:(MessageType)msgType {
    
    if (self = [super init]) {
        self.msgType = msgType;
        
    }
    
    return self;
}

-(XMPPMessage *)parseToXMPPMessage {
    
    XMPPMessage *aXmppMessage = [super parseToXMPPMessage];
    NSXMLElement * body = [NSXMLElement elementWithName:TAG_BODY stringValue:[Constants messageTypeStr:self.msgType]] ;
    [aXmppMessage addChild:body];
    [aXmppMessage addPropertyWithName:HRES_URL andValue:self.hresUrl andType:KTAG_STRING];
    [aXmppMessage addPropertyWithName:LRES_URL andValue:self.lresUrl andType:KTAG_STRING];
    
    
        
    return aXmppMessage;
    
    
}

-(NSString *)toString {
    //define this method to show what kind of string you need to send
    return [Constants getFiletype:self.msgType];
    
}


@end
