//
//  AppDelegate.m
//  XmppLib
//
//  Created by Babul Prabhakar on 18/04/15.
//  Copyright (c) 2015 Babul Prabhakar. All rights reserved.
//

#import "AppDelegate.h"
#import "XMPPController.h"
#import "ViewController.h"
#import "MessageController.h"
#import "Constants.h"
#import "DDLog.h"
#import "DDASLLogger.h"
#import "DDTTYLogger.h"
#define BABUL 1
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //Connect With Jid and Password
    [DDLog addLogger:[DDTTYLogger sharedInstance] withLogLevel:ddLogLevel];
  
    if (BABUL) {
        [[XMPPController  sharedSingleton] connectWithJID:@"9718520505" password:@"admin"];
      //  mainController.currentChat = [msgController createChatForJID:@"8595358379@"DOMAIN_NAME_XMPP_CHATNA"" withDisplayName:@"test 1"];
    } else {
        [[XMPPController  sharedSingleton] connectWithJID:@"8595358379" password:@"123456"];
    }
 
  
  
    
    
    
   // [[XMPPController sharedSingleton]connectWithJID:@"8595358379" password:@"admin"];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
