//
//  Constants.m
//  XmppLib
//
//  Created by Babul Prabhakar on 18/04/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import "Constants.h"

@implementation Constants
+(NSString*)bareJID:(NSString*)aNumber
{
    NSRange atRange = [aNumber rangeOfString:@"@"];
    if (atRange.location == NSNotFound)
    {
        return [NSString stringWithFormat:@"%@@%@", aNumber, DOMAIN_NAME_XMPP_CHATNA ];
    }
    return aNumber;
}

NSString *const MESSAGE_RECIEVED_NOTIFICATION = @"MESSAGE_RECIEVED_NOTIFICATION";



+(NSString *)getExtensionFromType:(MessageType )msgType {
    
    
    switch (msgType) {
        case IMAGE_TYPE_MESSAGE:
            return @"image/jpeg";
            break;
        case VIDEO_TYPE_MESSAGE:
            return @"video/mp4";
            break;
            
        case AUDIO_TYPE_MESSAGE:
            return @"audio/m4a";
            break;


        default:
            return nil;
            break;
    }
}

+(NSString *)getFiletype:(MessageType)msgType {
    
    switch (msgType) {
        case IMAGE_TYPE_MESSAGE:
            return @"image";
            break;
        case VIDEO_TYPE_MESSAGE:
            return @"video";
            break;
            
        case AUDIO_TYPE_MESSAGE:
            return @"audio";
            break;
            
            
        default:
            return nil;
            break;
    }
    
    
}



+(NSString *)messageTypeStr:(MessageType)msgType {
    
    
    switch (msgType) {
            case TEXT_TYPE_MESSAGE:
            return @"TEXT_TYPE_MESSAGE";
            
        case IMAGE_TYPE_MESSAGE:
            return @"IMAGE_TYPE_MESSAGE";
            break;
        case VIDEO_TYPE_MESSAGE:
            return @"VIDEO_TYPE_MESSAGE";
            break;
            
        case AUDIO_TYPE_MESSAGE:
            return @"AUDIO_TYPE_MESSAGE";
            break;
            
            
        default:
            return nil;
            break;
    }

    
}


+(MessageType)messageTypeFromStr:(NSString *)messageTypeStr {
    MessageType msgType = TEXT_TYPE_MESSAGE;
    
    if ([messageTypeStr isEqualToString:@"IMAGE_TYPE_MESSAGE"]) {
        msgType = IMAGE_TYPE_MESSAGE;
    } else if ([messageTypeStr isEqualToString:@"VIDEO_TYPE_MESSAGE"]) {
        msgType = VIDEO_TYPE_MESSAGE;
        
    }  else if ([messageTypeStr isEqualToString:@"AUDIO_TYPE_MESSAGE"]) {
        msgType = AUDIO_TYPE_MESSAGE;
        
    }
    
    return msgType;
    
}

int const HTTP_STATUS_SUCCESS = 200;

@end
