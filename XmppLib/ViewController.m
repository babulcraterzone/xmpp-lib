//
//  ViewController.m
//  XmppLib
//
//  Created by Babul Prabhakar on 18/04/15.
//  Copyright (c) 2015 Babul Prabhakar. All rights reserved.
//

#import "ViewController.h"
#import "Chat.h"
#import "TextMessage.h"
#import "FileMessage.h"
int const LIMIT = 20;
@interface ViewController ()<UIActionSheetDelegate,ChatMessageDelegate>

@end

@implementation ViewController


@synthesize currentChat = _currentChat;

- (void)viewDidLoad {
    [super viewDidLoad];
    _currentChat.chatDelegate = self;
    self.senderId = @"BP";
    self.senderDisplayName = @"Babul Prabhakar";
    self.messageArr = [NSMutableArray array];
    self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
    self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
    self.showLoadEarlierMessagesHeader = YES;

    JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
    
    self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
    self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleGreenColor]];
    
    
    //load Messages
    NSArray *messageArr = [_currentChat loadMessagesWithCurrentOffset:0 andLimit:LIMIT];
    [messageArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        JSQMessage *msg = [self addMessageToTable:(Message *)obj];
        [self.messageArr addObject:msg];
       
        
    }];
    self.automaticallyScrollsToMostRecentMessage = YES;
     [self finishSendingMessageAnimated:NO];
   
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveMessage:) name:MESSAGE_RECIEVED_NOTIFICATION object:nil];
    
    
}


-(void)didReceiveMessage:(NSNotification *)notification {
    
    if ([notification.object isKindOfClass:[Message class]]) {
        Message *message = (Message *)notification.object;
        JSQMessage *msg = [self addMessageToTable:message];
        [self.messageArr addObject:msg];
        [self finishSendingMessageAnimated:YES];
        
        [JSQSystemSoundPlayer jsq_playMessageReceivedAlert];
        }
    
}

-(JSQMessage *)addMessageToTable:(Message *)message {
    _START
    JSQMessage *jsqMessage;
    NSString *senderID;
    NSString *displayName;
    NSLog(@"%@",message.debugDescription);
    if (message.isOutgoing) {
        senderID = @"BP";
        displayName = @"Babul Prabhakar";
    } else {
        senderID = [_currentChat chatID];
        displayName = [_currentChat chatDisplayName];
    }
    
    if ([message isKindOfClass:[TextMessage class]]) {
        TextMessage *textMsg = (TextMessage *)message; // can be customized with diffrent typ of messages
       
        
        
        jsqMessage = [[JSQMessage alloc] initWithSenderId:senderID
                                        senderDisplayName:displayName
                                                     date:textMsg.messageDateAndTime
                                                     text:textMsg.body];
    } else if([message isKindOfClass:[FileMessage class]]) {
        FileMessage *fileMessage = (FileMessage *)message;
        JSQPhotoMediaItem *photoItem;
        if (fileMessage.isOutgoing) {
             photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageWithContentsOfFile:fileMessage.fileURI]];
        } else {
            
            photoItem = [[JSQPhotoMediaItem alloc]initWithURL:[NSURL URLWithString:fileMessage.lresUrl]];
                   }
       
    
        
        jsqMessage = [JSQMessage messageWithSenderId:senderID
                                         displayName:displayName
                                               media:photoItem];
        
    }
    return jsqMessage;
    
    
    
   
    _END
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MESSAGE_RECIEVED_NOTIFICATION object:nil];
    
    
}

#pragma mark - JSQMessagesViewController method overrides
- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
    /**
     *  Sending a message. Your implementation of this method should do *at least* the following:
     *
     *  1. Play sound (optional)
     *  2. Add new id<JSQMessageData> object to your data source
     *  3. Call `finishSendingMessage`
     */
    NSString *trimmedText = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (trimmedText.length > 0) {
        
        TextMessage *txtMessage = [[TextMessage alloc] init];
        txtMessage.body = text;
        [_currentChat sendMessage:txtMessage];
        
        JSQMessage *message = [[JSQMessage alloc] initWithSenderId:senderId
                                                 senderDisplayName:senderDisplayName
                                                              date:date
                                                              text:text];
        
        
       
        [self.messageArr addObject:message];
        self.automaticallyScrollsToMostRecentMessage = YES;
        [self finishSendingMessageAnimated:YES];
        
        [JSQSystemSoundPlayer jsq_playMessageSentAlert];
        
    }
  
}

- (void)finishSendingMessageAnimated:(BOOL)animated {
    
    UITextView *textView = self.inputToolbar.contentView.textView;
    textView.text = nil;
    [textView.undoManager removeAllActions];
    
    [self.inputToolbar toggleSendButtonEnabled];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:UITextViewTextDidChangeNotification object:textView];
    
    [self.collectionView.collectionViewLayout invalidateLayoutWithContext:[JSQMessagesCollectionViewFlowLayoutInvalidationContext context]];
    [self.collectionView reloadData];
    
    if (self.automaticallyScrollsToMostRecentMessage) {
        [self scrollToBottomAnimated:animated];
    }
}



#pragma mark - Collection View Delegates -
- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.messageArr objectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  You may return nil here if you do not want bubbles.
     *  In this case, you should set the background color of your collection view cell's textView.
     *
     *  Otherwise, return your previously created bubble image data objects.
     */
    
    JSQMessage *message = [self.messageArr objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.outgoingBubbleImageData;
    }
    
    return self.incomingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Return `nil` here if you do not want avatars.
     *  If you do return `nil`, be sure to do the following in `viewDidLoad`:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
     *
     *  It is possible to have only outgoing avatars or only incoming avatars, too.
     */
    
    /**
     *  Return your previously created avatar image data objects.
     *
     *  Note: these the avatars will be sized according to these values:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize
     *
     *  Override the defaults in `viewDidLoad`
     */
    return nil;
//    JSQMessage *message = [self.messageArr objectAtIndex:indexPath.item];
//    
//    if ([message.senderId isEqualToString:self.senderId]) {
//        if (![NSUserDefaults outgoingAvatarSetting]) {
//            return nil;
//        }
//    }
//    else {
//        if (![NSUserDefaults incomingAvatarSetting]) {
//            return nil;
//        }
//    }
//    
//    
//    return [self.demoData.avatars objectForKey:message.senderId];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
     *  The other label text delegate methods should follow a similar pattern.
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        JSQMessage *message = [self.messageArr objectAtIndex:indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messageArr objectAtIndex:indexPath.item];
    
    /**
     *  iOS7-style sender name labels
//     */
//    if ([message.senderId isEqualToString:self.senderId]) {
//        return nil;
//    }
//    
//    if (indexPath.item - 1 > 0) {
//        JSQMessage *previousMessage = [self.messageArr objectAtIndex:indexPath.item - 1];
//        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
//            return nil;
//        }
//    }
//    
//    /**
//     *  Don't specify attributes to use the defaults.
//     */
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.messageArr count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Override point for customizing cells
     */
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    /**
     *  Configure almost *anything* on the cell
     *
     *  Text colors, label text, label colors, etc.
     *
     *
     *  DO NOT set `cell.textView.font` !
     *  Instead, you need to set `self.collectionView.collectionViewLayout.messageBubbleFont` to the font you want in `viewDidLoad`
     *
     *
     *  DO NOT manipulate cell layout information!
     *  Instead, override the properties you want on `self.collectionView.collectionViewLayout` from `viewDidLoad`
     */
    
    JSQMessage *msg = [self.messageArr objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            cell.textView.textColor = [UIColor blackColor];
        }
        else {
            cell.textView.textColor = [UIColor whiteColor];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
        
    }
    
    return cell;
}



#pragma mark - JSQMessages collection view flow layout delegate

#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  iOS7-style sender name labels
     */
    JSQMessage *currentMessage = [self.messageArr objectAtIndex:indexPath.item];
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.messageArr objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.0f;
}

#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
{
    NSLog(@"Load earlier messages!");
    NSArray *messageArr = [_currentChat loadMessagesWithCurrentOffset:(int)self.messageArr.count  andLimit:LIMIT];
    if (messageArr.count < 20) {
        self.showLoadEarlierMessagesHeader = NO;
    }
    [messageArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        JSQMessage *msg = [self addMessageToTable:(Message *)obj];
        [self.messageArr insertObject:msg atIndex:idx];
        
        
    }];
    self.automaticallyScrollsToMostRecentMessage = NO;
    [self finishSendingMessageAnimated:NO];
    
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped message bubble!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation
{
    NSLog(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));
}


- (void)didPressAccessoryButton:(UIButton *)sender
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Media messages"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:@"Send photo", @"Send location", @"Send video", nil];
    
    [sheet showFromToolbar:self.inputToolbar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    switch (buttonIndex) {
        case 0:
           
            [self addImage];
            break;
            
        case 1:
        {
 
        }
            break;
            
        case 2:
           
            break;
    }
    
    [JSQSystemSoundPlayer jsq_playMessageSentAlert];
   
    [self finishSendingMessageAnimated:YES];
}

-(void)addImage {
    
    FileMessage *filemsg = [[FileMessage alloc] init];
    filemsg.msgType = IMAGE_TYPE_MESSAGE;
    filemsg.fileURI = [[[NSBundle mainBundle] resourcePath]  stringByAppendingPathComponent:@"IMG_0338.jpeg"];
  
    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@"IMG_0338.jpeg"]];
    JSQMessage *photoMessage = [JSQMessage messageWithSenderId:@"BP"
                                                   displayName:@"Babul Prabhakar"
                                                         media:photoItem];
    
   
    [_currentChat sendMessage:filemsg];
    
    [self.messageArr addObject:photoMessage];
    self.automaticallyScrollsToMostRecentMessage = YES;
    [self finishSendingMessageAnimated:YES];

}



#pragma mark - Chat message delegate -

-(void)handleMessageChange:(NSString *)aBareJid messageId:(NSString *)messageId status:(MessageStatus)newState {
    
    if ([aBareJid isEqual:_currentChat.jid]) {
        
        
        
        // do you message head updation
        
        
        
    }
    
    
    
    
    
    
}




@end
