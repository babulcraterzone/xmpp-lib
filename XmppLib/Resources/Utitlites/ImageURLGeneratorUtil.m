//
//  ImageURLGeneratorUtil.m
//  XmppLib
//
//  Created by Babul Prabhakar on 02/05/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import "ImageURLGeneratorUtil.h"
#import "Constants.h"
@implementation ImageURLGeneratorUtil
/*http://localhost:8080/content-service/v1/image/projectId/OTkwRFhYRS9vYzZaL0pwQm5JWi9jbXgrcUVWV1N0b1NIaVdtTVRkalkzNVNHdkI4QW96dUx6am9zM2Zsam1Ka3ovNm5id3IyNVRpK1B4cE9xUWFiSHJJVjA5ODFiNWtoT0tPNnJ5TlJNU3c9?thumb=false*/
+(NSString *)generateURLWithContentID:(NSString *)contentID isThumb:(BOOL)isThumb {
    
    NSMutableString *url = [[NSMutableString alloc] initWithString:HTTP_DEV_SERVER@"content-service-local/v1/image/projectId/"];
    [url appendString:contentID];
    NSString *boolStr = isThumb?@"true":@"false";
    [url appendFormat:@"?thumb=%@",boolStr];
    
    return url;
    
}
@end
