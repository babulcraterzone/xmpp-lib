//
//  WebserviceMapperUtil.h
//  XmppLib
//
//  Created by Babul Prabhakar on 01/05/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import <Foundation/Foundation.h>
@class FileUploadRequest;
typedef void (^FileUploadBlock)(id response, NSInteger error);
@interface WebserviceMapperUtil : NSObject
+(void)uploadImageOrVideo:(FileUploadRequest *)request WithBlock:(FileUploadBlock)block;
+(void)uploadAudio:(FileUploadRequest *)request WithBlock:(FileUploadBlock)block;
@end
