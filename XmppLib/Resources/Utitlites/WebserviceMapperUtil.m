//
//  WebserviceMapperUtil.m
//  XmppLib
//
//  Created by Babul Prabhakar on 01/05/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import "WebserviceMapperUtil.h"
#import "HTTPClientUtil.h"
#import "FileUploadRequest.h"
#import "Constants.h"
#import "FileUploadResponse.h"
#import <AFHTTPRequestOperationManager.h>

@implementation WebserviceMapperUtil


+(void)uploadImageOrVideo:(FileUploadRequest *)request WithBlock:(FileUploadBlock)block {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@content-service-local/v1/image/projectId?async=false&thumb=true",HTTP_DEV_SERVER];
    
    [HTTPClientUtil postMultiPartToWS:urlStr FileUploadReq:request WithHeaderDict:nil withBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (error == nil) {
            
            FileUploadResponse *response = [[FileUploadResponse alloc] initWithDict:operation.responseObject];
                         block(response,operation.response.statusCode);
                    }
    }];

    
}

+(void)uploadAudio:(FileUploadRequest *)request WithBlock:(FileUploadBlock)block {
    
     NSString *urlStr = [NSString stringWithFormat:@"%@/content-service-local/v1/image/projectId?async=false&thumb=false",HTTP_DEV_SERVER];
    [HTTPClientUtil postMultiPartToWS:urlStr FileUploadReq:request WithHeaderDict:nil withBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (error == nil) {
            block(operation.responseString,operation.response.statusCode);
        }
    }];
}




@end
