//
//  ImageURLGeneratorUtil.h
//  XmppLib
//
//  Created by Babul Prabhakar on 02/05/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageURLGeneratorUtil : NSObject


+(NSString *)generateURLWithContentID:(NSString *)contentID isThumb:(BOOL)isThumb;
@end
