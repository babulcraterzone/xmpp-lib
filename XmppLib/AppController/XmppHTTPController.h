//
//  HTTPController.h
//  XmppLib
//
//  Created by Babul Prabhakar on 01/05/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebserviceMapperUtil.h"
@class FileUploadRequest;
@interface XmppHTTPController : NSObject
+(instancetype)sharedSingleton;

-(void)uploadImageOrVideo:(FileUploadRequest *)request WithBlock:(FileUploadBlock)block;
-(void)uploadAudio:(FileUploadRequest *)request WithBlock:(FileUploadBlock)block;
@end
