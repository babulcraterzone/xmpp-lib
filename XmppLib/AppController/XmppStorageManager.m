//
//  StorageManager.m
//  XmppLib
//
//  Created by Babul Prabhakar on 30/04/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import "XmppStorageManager.h"
#import "XMPPMessageArchivingCoreDataStorage.h"
#import "Message.h"
#import "Chat.h"
#import "XMPPController.h"
@interface XmppStorageManager()<NSFetchedResultsControllerDelegate> {
    NSFetchedResultsController*  _contactsController;
}

@end
@implementation XmppStorageManager
+(instancetype)sharedSingleton {
    static XmppStorageManager *sharedSingleton;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSingleton = [[XmppStorageManager alloc] init];
    });
    
    return sharedSingleton;
    
}





-(NSArray *)loadMessagesForJid:(NSString *)jid withLimit:(int)limit withOffset:(int)offset {
    
    XMPPMessageArchivingCoreDataStorage *storage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [storage messageEntity:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
   NSString *predicateFrmt = @"bareJidStr == %@";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt, jid];
    request.predicate = predicate;
    [request setEntity:entityDescription];
    [request setFetchLimit:limit];
    [request setFetchOffset:offset];
    
    [request setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:NO]]];
    NSError *error;
    NSArray *messages = [moc executeFetchRequest:request error:&error];
  NSSortDescriptor *sort =  [[NSSortDescriptor alloc]initWithKey:@"timestamp" ascending:YES];
    NSArray *descArr =  [messages sortedArrayUsingDescriptors:@[sort]];
    NSArray *returningArr = [Message  arrayOfMessageFromXMPPMessageArchivingCoreDataStorage:descArr];
   
    
    return returningArr;
    
  
}



-(NSDictionary *)loadChatList {
    
    
    XMPPMessageArchivingCoreDataStorage *storage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [storage contactEntity:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDescription];
     NSSortDescriptor *sort =  [[NSSortDescriptor alloc]initWithKey:@"mostRecentMessageTimestamp" ascending:NO];
    [request setSortDescriptors:@[sort]];
    _contactsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:moc sectionNameKeyPath:nil cacheName:@"MessagesContactListCache"];
    //_contactsController.delegate = self;//causing crash dont use it.
    NSError *error;
    [_contactsController performFetch:&error];
   NSDictionary *chatList = [Chat arrayOfChatFromXMPPMessageArchiving_Contact_CoreDataObject:_contactsController.fetchedObjects];
    
         
    return chatList;
    
}



- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    
    
    
    
    
    
}


#pragma mark - Storage Message - 


-(void)saveOrUpdateMessage:(Message *)message {
  
    XMPPMessageArchivingCoreDataStorage *storage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    [storage archiveMessage:message xmppStream:[XMPPController sharedSingleton].xmppStream];
    
}
-(NSArray *)loadMessageWithState:(NSArray *)statusArray {
    
    
    XMPPMessageArchivingCoreDataStorage *storage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    NSManagedObjectContext *moc = [storage mainThreadManagedObjectContext];
    NSEntityDescription *entityDescription = [storage messageEntity:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    NSMutableString *predicateFrmt = [[NSMutableString alloc] initWithFormat:@"("];
    
    for (NSNumber *number in statusArray) {
        [predicateFrmt appendFormat:@"messageState == %d || ",[number intValue] ];
    }
   
    NSRange lastComma = [predicateFrmt rangeOfString:@" || " options:NSBackwardsSearch];
    
    if(lastComma.location != NSNotFound) {
        NSString *tempStr = [predicateFrmt stringByReplacingCharactersInRange:lastComma
                                           withString: @")"];
        predicateFrmt = [[NSMutableString alloc] initWithString:tempStr];
    }
    [predicateFrmt appendFormat:@" &&  outgoing == %d",YES];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFrmt];
    request.predicate = predicate;
    [request setEntity:entityDescription];
   
    
    [request setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:NO]]];
    NSError *error;
    NSArray *messages = [moc executeFetchRequest:request error:&error];
    NSSortDescriptor *sort =  [[NSSortDescriptor alloc]initWithKey:@"timestamp" ascending:YES];
    NSArray *descArr =  [messages sortedArrayUsingDescriptors:@[sort]];
    NSArray *returningArr = [Message  arrayOfMessageFromXMPPMessageArchivingCoreDataStorage:descArr];
    
    
    return returningArr;

    
    
}
@end
