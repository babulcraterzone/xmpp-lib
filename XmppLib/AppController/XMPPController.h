//
//  XMPPController.h
//  XmppLib
//
//  Created by Babul Prabhakar on 18/04/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMPPStream.h"
#import "XMPPFramework.h"
#import "XMPPReconnect.h"
#import "XMPPCapabilities.h"
#import "XMPPCapabilitiesCoreDataStorage.h"
#import "XMPPMessage+XEP_0085.h"
#import "XMPPMessage+XEP_0184.h"
@class MessageController;


@interface XMPPController : NSObject {
    
    XMPPJID *jid;
    NSString *_password;
    XMPPReconnect *_xmppReconnect;
    XMPPCapabilities *_xmppCapabilities;
    XMPPCapabilitiesCoreDataStorage *_xmppCapabilitiesStorage;

    
}
@property (nonatomic,strong,readonly)XMPPStream *xmppStream;
@property (nonatomic,strong)MessageController *messageController;

+(instancetype)sharedSingleton;

/*!
 *@author Babul Prabhakar
 * @discussion Connection Method
 * @param jid and password
 * @return bool
 */
- (BOOL)connectWithJID:(NSString*)myJID password:(NSString*)myPassword;

/*!
 *@author Babul Prabhakar
 * @discussion check if service connected
 * @param void
 * @return Bool 
 */
-(BOOL) isServiceConnected;
@end
