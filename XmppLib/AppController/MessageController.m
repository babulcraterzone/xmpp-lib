//
//  MessageController.m
//  XmppLib
//
//  Created by Babul Prabhakar on 18/04/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import "MessageController.h"
#import "XMPPMessageDeliveryReceipts.h"
#import "Message.h"
#import "TextMessage.h"
#import "XMPPMessage.h"
#import "Chat.h"
#import "XMPPController.h"
#import "XMPPMessageArchivingCoreDataStorage.h"
#import "FileMessage.h"
#import "FileUploadRequest.h"
#import "XmppHTTPController.h"
#import "FileUploadResponse.h"
#import "ImageURLGeneratorUtil.h"
#import "XmppStorageManager.h"
@interface MessageController() {
    
 
}

@end

@implementation MessageController
@synthesize xmppMessageArchivingModule;
@synthesize xmppMessageArchivingStorage;
#pragma mark - Initialization
-(instancetype)initWithStream:(XMPPStream *)xmppStream {
    
    if (self = [super init]) {
        _xmppstream = xmppStream;
        [self setupController];
    }
    return self;
    
}



-(void)setupController {
    //init message  controller properties
    [self getAllChatList];
  
    _allChatList  = [NSMutableDictionary dictionaryWithDictionary: [[XmppStorageManager sharedSingleton] loadChatList]];
    
    _allWaitingMessageQueue = [[NSMutableArray alloc] init];
    //for MDN
    XMPPMessageDeliveryReceipts* xmppMessageDeliveryRecipts = [[XMPPMessageDeliveryReceipts alloc] init];
    xmppMessageDeliveryRecipts.autoSendMessageDeliveryReceipts = YES;
    xmppMessageDeliveryRecipts.autoSendMessageDeliveryRequests = YES;
   
    [xmppMessageDeliveryRecipts addDelegate:self delegateQueue:dispatch_get_main_queue()];
    [xmppMessageDeliveryRecipts activate:_xmppstream];
   
    
    //message archiving to store message in core data provided by xmpp framework
    xmppMessageArchivingStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    xmppMessageArchivingModule = [[XMPPMessageArchiving alloc] initWithMessageArchivingStorage:xmppMessageArchivingStorage];
    [xmppMessageArchivingModule setClientSideMessageArchivingOnly:YES];
    [xmppMessageArchivingModule activate:_xmppstream];
    [xmppMessageArchivingModule  addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    
    //add delegate of xmppstream to get stream method called
    [_xmppstream addDelegate:self delegateQueue:dispatch_get_main_queue()];
}



-(void) handleServiceAuthenticated
{
    NSArray *pendingArray = [[XmppStorageManager sharedSingleton] loadMessageWithState:@[[NSNumber numberWithInt:MESSAGE_STATUS_WAITING],[NSNumber numberWithInt:MESSAGE_STATUS_FAILED]]];
    _allWaitingMessageQueue = [NSMutableArray arrayWithArray:pendingArray];
    [self asynchSendQueueMessage];
    
}

/*
 *call it in delloc of to tear down connection
 */
- (void)teardown
{
    
}
#pragma mark - 

#pragma mark - Message Sending and Recieving
- (void)xmppStream:(XMPPStream *)sender didSendMessage:(XMPPMessage *)message {
    MessageStatus msgStatus = MESSAGE_STATUS_SERVER;
    
    //post notification for  delivered  notification
    
    //dont need to save here already done changes in xep0136  -- Babul
    Chat* chat = [self getChatForJID:[message.to bare]];
    if( chat )
    {
        NSString *msgID = [message elementID];
        if ([chat.chatDelegate conformsToProtocol:@protocol(ChatMessageDelegate)] && [chat.chatDelegate respondsToSelector:@selector(handleMessageChange:messageId:status:)]) {
            [chat.chatDelegate handleMessageChange:[message.to bare] messageId:msgID status:msgStatus];
        }
    }
    
    
}
- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message {
    _START
    if ([message isMessageWithBody] && ![message isErrorMessage]) {
    Message *msg = [[Message alloc]initWithXmppMessage:message];
    if (msg.msgType == TEXT_TYPE_MESSAGE)
    {
        msg = [[TextMessage alloc]initWithXmppMessage:message];
    }
    else if (msg.msgType == IMAGE_TYPE_MESSAGE) {
        msg = [[FileMessage alloc] initWithXmppMessage:message];
        
    }
        // message
        if( msg )
        {
            Chat* chat = [self createChatForJID:msg.msgJid withDisplayName:message.fromStr];
            if( chat ){
                [chat handleRecievedMessage:msg];
                
            }
        }
        
    } else if ([message hasReceiptResponse] && ![message isErrorMessage]) {
        
        MessageStatus msgStatus = MESSAGE_STATUS_USER;
        [[XMPPMessageArchivingCoreDataStorage sharedInstance] archiveMessage:message outgoing:YES xmppStream:_xmppstream WithState:msgStatus];
        //post notification for  delivered  notification
       
        Chat* chat = [self getChatForJID:[message.from bare]];
        if( chat )
        {
            NSString *msgID = [[[message elementForName:@"received"] attributeForName:@"id"] stringValue];
            if ([chat.chatDelegate conformsToProtocol:@protocol(ChatMessageDelegate)] && [chat.chatDelegate respondsToSelector:@selector(handleMessageChange:messageId:status:)]) {
                [chat.chatDelegate handleMessageChange:[message.from bare] messageId:msgID status:msgStatus];
            }
        }
    }
    _END
}

-(void)sendMessage:(Message *)message {
    
    if (message) {
        XMPPMessage * aXmppMessage = [message parseToXMPPMessage];
        [_xmppstream sendElement:aXmppMessage];
    }

}

-(void)sendOrQueueChatMessage:(Message *)message
{
    if( [[XMPPController sharedSingleton]  isServiceConnected] && !_isMessageSending)
    {
        [self processSendMessage:message];
    }
    else
    {
        [self queueMessage:message];
    }
}

-(void) queueMessage:(Message*) chatMessage
{
    [_allWaitingMessageQueue addObject:chatMessage];
}


-(void) processSendMessage:(Message*) message
{
    //  check connection here
    if( message.msgType == TEXT_TYPE_MESSAGE  )
    {
        [self doSendChatMessage:message withErr:0];
    }
    else {
        FileMessage *fileMessage = (FileMessage *)message;
        //file upload
        
        FileUploadRequest *fileupload = [[FileUploadRequest alloc] init];
        fileupload.exten = [Constants getExtensionFromType:message.msgType];
        fileupload.type = [Constants getFiletype:message.msgType];
        UIImage *img = [UIImage imageWithContentsOfFile:fileMessage.fileURI];
        NSAssert(img != nil, @"Image is nil please check for the file uri");
        fileupload.data = UIImageJPEGRepresentation(img, COMPRESSION_STRENGTH); // update according to need
        fileupload.fileName = [fileMessage.fileURI lastPathComponent];
        
        if(message.msgType == IMAGE_TYPE_MESSAGE || message.msgType == VIDEO_TYPE_MESSAGE )
        {
            [[XmppHTTPController sharedSingleton] uploadImageOrVideo:fileupload WithBlock:^(id response, NSInteger statusCode) {
                
                if (statusCode == HTTP_STATUS_SUCCESS) {
                    [self handleFileUploadResponse:response WithMessage:fileMessage];
                }

            }];
            
        } else if (message.msgType == AUDIO_TYPE_MESSAGE) {
            [[XmppHTTPController sharedSingleton] uploadAudio:fileupload WithBlock:^(id response, NSInteger statusCode) {
                if (statusCode == HTTP_STATUS_SUCCESS) {
                    [self handleFileUploadResponse:response WithMessage:fileMessage];
                }

            }];
            
        }
    
    }
   
}


-(void) handleFileUploadResponse:(FileUploadResponse*)aFileRespone WithMessage:(FileMessage *)message
{
    _isMessageSending = NO;
    
    if( !aFileRespone ){
        [self asynchSendQueueMessage];
        return;
    }
    
   
    if( message )
    {
        message.hresUrl = [ImageURLGeneratorUtil generateURLWithContentID:aFileRespone.contentId isThumb:NO];
        message.lresUrl = [ImageURLGeneratorUtil generateURLWithContentID:aFileRespone.contentId isThumb:YES];
        [self doSendChatMessage:message withErr:0];
       
    }
   
}

-(void) asynchSendQueueMessage
{
    if( [_allWaitingMessageQueue count] && !_isMessageSending)
    {
        //start timer
         _isMessageSending = YES;
        [self sendQueueMessage:nil];
       
    }
}

-(void) sendQueueMessage:(NSTimer*) aTimer
{
    Message* firstMessage = [_allWaitingMessageQueue firstObject];
    
    [self processSendMessage:firstMessage];
    
}


-(Chat*) getChatForJID:(NSString*) Jid
{
    Chat* chat = [_allChatList objectForKey:Jid];
    return chat;
}


-(void)doSendChatMessage:(Message*) aMessage withErr:(NSInteger) error
{
    
    Chat* chat = [self getChatForJID:aMessage.msgJid];
    if( error >=0 )
    {
        // this implies that it is one to one chat   -- babul
        //        if (![aMessage.bareJid rangeOfString:@"conference"].location == NSNotFound) {
        [self sendMessage:aMessage];
       // aMessage.me = SEND;
    }
    else
    {
      //  aMessage.messageDeliveryStatus = NOT_SEND;
    }
    
//    if (aMessage.chatType ==IMAGE)
//    {
//        FileMessage *fileMessage = (FileMessage *)aMessage;
//        [[StorageManager sharedInstance] updateFileMessage:fileMessage];
//    }
    
    
    
    [_allWaitingMessageQueue removeObject:aMessage];
    if( chat ){
//        if ([chat.chatDelegate conformsToProtocol:@protocol(ChatMessageDelegate)] && [chat.chatDelegate respondsToSelector:@selector(handleMessageSent:)]) {
//            // [chat.chatDelegate handleMessageSent:aMessage];
//        }
    }
    _isMessageSending = NO;
    [self asynchSendQueueMessage];
}



#pragma mark -



-(Chat*) createChatForJID:(NSString*) Jid withDisplayName:(NSString*) aName
{
    if( Jid == nil )
        return nil;
    Chat* chat = [_allChatList objectForKey:Jid];
    if( !chat )
    {
        chat = [[Chat alloc]initWithChatJID:Jid WithName:aName];
        chat.chatId = [_xmppstream generateUUID];
        [_allChatList setObject:chat forKey:Jid];
        [[NSNotificationCenter defaultCenter]postNotificationName:UPDATE_CHAT_LIST object:nil];

    }
    if (aName) {
        [chat setChatDisplayName:aName];
    }
    return chat;
}


-(NSArray *)getAllChatList {
    
        return _allChatList.allValues;
}

@end


