//
//  XMPPController.m
//  XmppLib
//
//  Created by Babul Prabhakar on 18/04/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import "XMPPController.h"
#import "DDLog.h"
#import "Constants.h"
#import "MessageController.h"
@implementation XMPPController
@synthesize xmppStream = _xmppStream;
@synthesize messageController = _messageController;
+(XMPPController *)sharedSingleton {
    static XMPPController *sharedSingleton;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSingleton = [[XMPPController alloc] init];
        [sharedSingleton setupStream];
    });
    
    return sharedSingleton;

}



- (void)setupStream
{
   
    NSAssert(_xmppStream == nil, @"Method setupStream invoked multiple times");
    // Setup xmpp stream
    //
    // The XMPPStream is the base class for all activity.
    // Everything else plugs into the xmppStream, such as modules/extensions and delegates.
    
    _xmppStream = [[XMPPStream alloc] init];
    
#if !TARGET_IPHONE_SIMULATOR
    {
        // Want xmpp to run in the background?
        //
        // P.S. - The simulator doesn't support backgrounding yet.
        //        When you try to set the associated property on the simulator, it simply fails.
        //        And when you background an app on the simulator,
        //        it just queues network traffic til the app is foregrounded again.
        //        We are patiently waiting for a fix from Apple.
        //        If you do enableBackgroundingOnSocket on the simulator,
        //        you will simply see an error message from the xmpp stack when it fails to set the property.
        
        _xmppStream.enableBackgroundingOnSocket = YES;
    }
#endif
    
    // Setup reconnect
    //
    // The XMPPReconnect module monitors for "accidental disconnections" and
    // automatically reconnects the stream for you.
    // There's a bunch more information in the XMPPReconnect header file.
    
    _xmppReconnect = [[XMPPReconnect alloc] init];
    
    // Setup roster
    //
    // The XMPPRoster handles the xmpp protocol stuff related to the roster.
    // The storage for the roster is abstracted.
    // So you can use any storage mechanism you want.
    // You can store it all in memory, or use core data and store it on disk, or use core data with an in-memory store,
    // or setup your own using raw SQLite, or create your own storage mechanism.
    // You can do it however you like! It's your application.
    // But you do need to provide the roster with some storage facility.
    
    //DDLogVerbose(@"Unique Identifier: %@",self.account.uniqueIdentifier);
    
    //xmppRosterStorage = [[XMPPRosterCoreDataStorage alloc] initWithDatabaseFilename:self.account.uniqueIdentifier];
    //  xmppRosterStorage = [[XMPPRosterCoreDataStorage alloc] init];
    
    // Setup vCard support
    //
    // The vCard Avatar module works in conjuction with the standard vCard Temp module to download user avatars.
    // The XMPPRoster will automatically integrate with XMPPvCardAvatarModule to cache roster photos in the roster.
    
    // Setup capabilities
    //
    // The XMPPCapabilities module handles all the complex hashing of the caps protocol (XEP-0115).
    // Basically, when other clients broadcast their presence on the network
    // they include information about what capabilities their client supports (audio, video, file transfer, etc).
    // But as you can imagine, this list starts to get pretty big.
    // This is where the hashing stuff comes into play.
    // Most people running the same version of the same client are going to have the same list of capabilities.
    // So the protocol defines a standardized way to hash the list of capabilities.
    // Clients then broadcast the tiny hash instead of the big list.
    // The XMPPCapabilities protocol automatically handles figuring out what these hashes mean,
    // and also persistently storing the hashes so lookups aren't needed in the future.
    //
    // Similarly to the roster, the storage of the module is abstracted.
    // You are strongly encouraged to persist caps information across sessions.
    //
    // The XMPPCapabilitiesCoreDataStorage is an ideal solution.
    // It can also be shared amongst multiple streams to further reduce hash lookups.
    
    _xmppCapabilitiesStorage = [XMPPCapabilitiesCoreDataStorage sharedInstance];
    _xmppCapabilities = [[XMPPCapabilities alloc] initWithCapabilitiesStorage:_xmppCapabilitiesStorage];
    
    _xmppCapabilities.autoFetchHashedCapabilities = YES;
    _xmppCapabilities.autoFetchNonHashedCapabilities = NO;
    
    // Activate xmpp modules
    [_xmppReconnect         activate:_xmppStream];
    [_xmppCapabilities      activate:_xmppStream];
    
    // Add ourself as a delegate to anything we may be interested in
    
    [_xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    [_xmppCapabilities addDelegate:self delegateQueue:dispatch_get_main_queue()];
    [_xmppReconnect addDelegate:self delegateQueue:dispatch_get_main_queue()];
    _messageController = [[MessageController alloc] initWithStream:_xmppStream];
}



- (BOOL)connectWithJID:(NSString*)myJID password:(NSString*)myPassword
{
    _START;
    myJID = [Constants bareJID:myJID];
    
    
    if (![_xmppStream isDisconnected]) {
        return YES;
    }
    
   
    
    //
    // If you don't want to use the Settings view to set the JID,
    // uncomment the section below to hard code a JID and password.
    //
    // Replace me with the proper JID and password:
    //	myJID = @"user@gmail.com/xmppframework";
    //	myPassword = @"";
    //myJID = @"test@niravgupta";
    
    
    if (myJID == nil || myPassword == nil) {
        DDLogWarn(@"JID and password must be set before connecting!");
        
        return NO;
    }
    _password = myPassword;
    int r = arc4random() % 99999;
    
    NSString * resource = [NSString stringWithFormat:@"%@%d",CHATNA_RESOURCE,r];
    
    jid = [XMPPJID jidWithString:myJID resource:resource];
    
    [_xmppStream setMyJID:jid];
    [_xmppStream setHostName:DOMAIN_NAME_XMPP_CHATNA];
    [_xmppStream setHostPort:PORT_NO_XMPP];
    _password = myPassword;
   
    NSLog(@"Authenticating with full JID = %@", jid.full);
    
    
    NSError *error = nil;
    if (![_xmppStream connectWithTimeout:XMPPStreamTimeoutNone error:&error])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error connecting"
                                                            message:@"See console for error details."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
        DDLogWarn(@"Error connecting: %@", error);
        
        return NO;
    }
    
    return YES;
}
-(BOOL) isServiceConnected
{
    if( _xmppStream )
        return [_xmppStream isConnected];
    
    return NO;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Core Data
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSManagedObjectContext *)managedObjectContext_capabilities
{
    return [_xmppCapabilitiesStorage mainThreadManagedObjectContext];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark XMPPStream Delegate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)xmppStream:(XMPPStream *)sender socketDidConnect:(GCDAsyncSocket *)socket
{
    
}

- (void)xmppStream:(XMPPStream *)sender willSecureWithSettings:(NSMutableDictionary *)settings
{
//    NSString *expectedCertName = [_xmppStream.myJID domain];
//    if (expectedCertName)
//    {
//        [settings setObject:expectedCertName forKey:(NSString *)kCFStreamSSLPeerName];
//    }
//    if( _useCustomCertificate )
//    {
//        [settings setObject:@(YES) forKey:GCDAsyncSocketManuallyEvaluateTrust];
//    }
}

/**
 * Allows a delegate to hook into the TLS handshake and manually validate the peer it's connecting to.
 *
 * This is only called if the stream is secured with settings that include:
 * - GCDAsyncSocketManuallyEvaluateTrust == YES
 * That is, if a delegate implements xmppStream:willSecureWithSettings:, and plugs in that key/value pair.
 *
 * Thus this delegate method is forwarding the TLS evaluation callback from the underlying GCDAsyncSocket.
 *
 * Typically the delegate will use SecTrustEvaluate (and related functions) to properly validate the peer.
 *
 * Note from Apple's documentation:
 *   Because [SecTrustEvaluate] might look on the network for certificates in the certificate chain,
 *   [it] might block while attempting network access. You should never call it from your main thread;
 *   call it only from within a function running on a dispatch queue or on a separate thread.
 *
 * This is why this method uses a completionHandler block rather than a normal return value.
 * The idea is that you should be performing SecTrustEvaluate on a background thread.
 * The completionHandler block is thread-safe, and may be invoked from a background queue/thread.
 * It is safe to invoke the completionHandler block even if the socket has been closed.
 *
 * Keep in mind that you can do all kinds of cool stuff here.
 * For example:
 *
 * If your development server is using a self-signed certificate,
 * then you could embed info about the self-signed cert within your app, and use this callback to ensure that
 * you're actually connecting to the expected dev server.
 *
 * Also, you could present certificates that don't pass SecTrustEvaluate to the client.
 * That is, if SecTrustEvaluate comes back with problems, you could invoke the completionHandler with NO,
 * and then ask the client if the cert can be trusted. This is similar to how most browsers act.
 *
 * Generally, only one delegate should implement this method.
 * However, if multiple delegates implement this method, then the first to invoke the completionHandler "wins".
 * And subsequent invocations of the completionHandler are ignored.
 **/
- (void)xmppStream:(XMPPStream *)sender didReceiveTrust:(SecTrustRef)trust
 completionHandler:(void (^)(BOOL shouldTrustPeer))completionHandler
{
    // The delegate method should likely have code similar to this,
    // but will presumably perform some extra security code stuff.
    // For example, allowing a specific self-signed certificate that is known to the app.
    
    
    dispatch_queue_t bgQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(bgQueue, ^{
        
        SecTrustResultType result = kSecTrustResultDeny;
        OSStatus status = SecTrustEvaluate(trust, &result);
        
        if (status == noErr && (result == kSecTrustResultProceed || result == kSecTrustResultUnspecified)) {
            completionHandler(YES);
        }
        else {
            completionHandler(NO);
        }
    });
}

#pragma mark - XMPPSTREAM Delegate methods
- (void)xmppStreamDidConnect:(XMPPStream *)sender
{
    _START;
    NSError *error = nil;
    if ([_xmppStream authenticateWithPassword:_password error:&error])
    {
        return;
    }
    _END;
}

- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender
{  _START
    [self goOnline];
    _END
}

- (void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(NSXMLElement *)error
{
  
    NSLog(@"Did Not Authenticate %@",error);
}

/*
 *@author - Babul Prabhakar
 *this delegate method is used to check if it send Presence
 */
- (void)xmppStreamConnectDidTimeout:(XMPPStream *)sender {
    
    NSLog(@"Connection time Out");
}


/* @author - Babul Prabhakar
 *method used to disconnect  should be called if no longer need of xmpp
 *
 */
- (void)disconnect
{
    [self goOffline];
    [_xmppStream disconnect];
}


/* @author - Babul Prabhakar
 * go offline method
 *
 */
- (void)goOffline
{
    XMPPPresence *presence = [XMPPPresence presenceWithType:@"unavailable"];
    [_xmppStream sendElement:presence];
}


/* @author - Babul Prabhakar
 * go online method
 */
- (void)goOnline
{
    
    XMPPPresence *presence = [XMPPPresence presenceWithType:@"available"];
    [_xmppStream sendElement:presence];
//    [_rosterController handleServiceAuthenticated];
    [_messageController handleServiceAuthenticated];
//    [_chatRoomController handleServiceAuthenticated];
  //  [[NSNotificationCenter defaultCenter]
  //   postNotificationName:UPDATE_FRIEND_LIST object:nil];
}

-(void)reconnect {
    if ([_xmppStream isDisconnected])
    {
        //[self setupStream];
        [self performReLogin];
       
        
    }
    else
    {
        DDLogVerbose(@"XMPP Connected again");
        
        [self goOnline];
        
    }
}
-(void)performReLogin {
    
//    NSString *xabberId = [[SettingsController sharedInstance] getXabberID:@""];
//    NSString *pwd = [[SettingsController sharedInstance] getPassword:@""];
//    if ([self connectWithJID:xabberId password:pwd]) {
//        NSLog(@"Login Succesful");
//    } else {
//        NSLog(@"UnsucessFull");
//    }
}

- (void)xmppStreamDidSecure:(XMPPStream *)sender
{
    NSLog(@"did secure ");
}


- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq
{
    if( [[iq type] isEqualToString:@"result"])
    {
        NSXMLElement *queryElement = [iq elementForName: @"query" xmlns: @"jabber:iq:search"];
        if (queryElement)
        {
            NSMutableArray *mArray = [[NSMutableArray alloc] init];
            
            NSXMLElement *xElement = [queryElement elementForName: @"x" xmlns: @"jabber:iq:data"];
            if( xElement )
            {
                NSArray *itemElements = [xElement elementsForName: @"item"];
                for (int i=0; i<[itemElements count]; i++)
                {
                    NSString *jidStr=[[[itemElements objectAtIndex:i] attributeForName:@"jid"] stringValue];
                    [mArray addObject:jidStr];
                }
            }
//            if( _rosterController )
//                [_rosterController handleFriendSearched:mArray];
        }
    }
    else if( [[iq type] isEqualToString:@"error"])
    {
        // notify about errors
    }
    return TRUE;
}

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
//    if( _rosterController )
//        [_rosterController handlePresenceChanged:presence];
}
- (void)xmppStream:(XMPPStream *)sender didReceiveError:(NSXMLElement *)error
{
    
}
/**
 * These methods are called after their respective XML elements are sent over the stream.
 * These methods may be used to listen for certain events (such as an unavailable presence having been sent),
 * or for general logging purposes. (E.g. a central history logging mechanism).
 **/
- (void)xmppStream:(XMPPStream *)sender didSendIQ:(XMPPIQ *)iq
{_START
    _END
    
    
}

- (void)xmppStream:(XMPPStream *)sender didSendMessage:(XMPPMessage *)message
{
    _START
    _END
    
}

- (void)xmppStream:(XMPPStream *)sender didSendPresence:(XMPPPresence *)presence
{
    _START
    _END
    
    //DDLogVerbose(@"Presence Send");
    //if( _rosterController )
    //  [_rosterController handlePresenceChanged:presence];
}

/**
 * These methods are called after failing to send the respective XML elements over the stream.
 **/
- (void)xmppStream:(XMPPStream *)sender didFailToSendIQ:(XMPPIQ *)iq error:(NSError *)error
{
    _START
    _END
    
}
- (void)xmppStream:(XMPPStream *)sender didFailToSendMessage:(XMPPMessage *)message error:(NSError *)error
{
    _START
    _END
    
}
- (void)xmppStream:(XMPPStream *)sender didFailToSendPresence:(XMPPPresence *)presence error:(NSError *)error
{
    _START
    _END
}



@end
