//
//  HTTPController.m
//  XmppLib
//
//  Created by Babul Prabhakar on 01/05/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import "XmppHTTPController.h"
#import "FileUploadRequest.h"
#import "WebserviceMapperUtil.h"
@implementation XmppHTTPController
+(instancetype)sharedSingleton {
    static XmppHTTPController *sharedSingleton;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSingleton = [[XmppHTTPController alloc] init];
    });
    
    return sharedSingleton;
    
}


-(void)uploadImageOrVideo:(FileUploadRequest *)request WithBlock:(FileUploadBlock)block {
    
    [WebserviceMapperUtil uploadImageOrVideo:request WithBlock:^(id response, NSInteger error) {
        block(response,error);
    }];
    
}
-(void)uploadAudio:(FileUploadRequest *)request WithBlock:(FileUploadBlock)block {
    
    [WebserviceMapperUtil uploadImageOrVideo:request WithBlock:^(id response, NSInteger error) {
        block(response,error);
    }];
}




@end
