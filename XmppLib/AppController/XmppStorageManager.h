//
//  StorageManager.h
//  XmppLib
//
//  Created by Babul Prabhakar on 30/04/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
@class Message;
@interface XmppStorageManager : NSObject
+(instancetype)sharedSingleton;

-(NSArray *)loadMessagesForJid:(NSString *)jid withLimit:(int)limit withOffset:(int)offset ;
-(NSDictionary *)loadChatList;
-(void)saveOrUpdateMessage:(Message *)message;
-(NSArray *)loadMessageWithState:(NSArray *)statusArray;
@end
