//
//  MessageController.h
//  XmppLib
//
//  Created by Babul Prabhakar on 18/04/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMPPStream.h"
@class Message;
@class Chat;
@class XMPPMessageArchivingCoreDataStorage;
@class XMPPMessageArchiving;
@interface MessageController : NSObject {
    
    XMPPStream *_xmppstream;
    NSMutableDictionary *_allChatList;
    NSMutableArray *_allWaitingMessageQueue;
     BOOL _isMessageSending;
    
}
@property (nonatomic,strong,readonly) XMPPMessageArchivingCoreDataStorage *  xmppMessageArchivingStorage;
@property (nonatomic,strong,readonly)XMPPMessageArchiving *xmppMessageArchivingModule;
/*!
 *@author Babul Prabhakar
 * @discussion checks if Chat Obj present in cache if yes then returns that Obj else Create new
 * @param Chat Jid And Display name
 * @return Chat obj
 */
-(Chat*) createChatForJID:(NSString*) Jid withDisplayName:(NSString*) aName;

-(instancetype)initWithStream:(XMPPStream *)xmppStream;
/*!
 *@author Babul Prabhakar
 * @discussion handles Service Authentication
 * @param void
 * @return void
 */
-(void) handleServiceAuthenticated;

- (void)teardown;
/*!
 *@author Babul Prabhakar
 * @discussion Use this method to send Message first queues the obj then when finds service connected sends the method
 * @param Message
 * @return void
 */
-(void)sendOrQueueChatMessage:(Message *)message;


-(NSArray *)getAllChatList;
@end
