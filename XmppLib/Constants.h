//
//  Constants.h
//  XmppLib
//
//  Created by Babul Prabhakar on 18/04/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import <Foundation/Foundation.h>
#define DOMAIN_NAME_XMPP_CHATNA @"178.62.225.251"
#define HTTP_DEV_SERVER @"http://95.85.17.164:8080/"
#define PORT_NO_XMPP 5222
#define CHATNA_RESOURCE @"iosxmppframework"
#define TAG_MESSAGE @"message"
#define TAG_BODY @"body"

#define TAG_TYPE @"type"
#define KTAG_PROPERTIES @"properties"
#define KTAG_PROPERTY  @"property"
#define KTAG_NAME       @"name"
#define KTAG_VALUE      @"value"
#define KTAG_TYPE      @"type"
#define MESSAGE_TYPE @"messageType"
#define KTAG_INTEGER     @"integer"
#define HRES_URL @"hres"
#define LRES_URL @"lres"
#define MESSAGE_STATE @"'msgstate"
#define KTAG_STRING      @"string"
#define COMPRESSION_STRENGTH 0.1


@interface Constants : NSObject
extern int const HTTP_STATUS_SUCCESS;
+(NSString*)bareJID:(NSString*)aNumber;



enum {
    TEXT_TYPE_MESSAGE = 0,
    IMAGE_TYPE_MESSAGE = 1,
    VIDEO_TYPE_MESSAGE = 2,
    AUDIO_TYPE_MESSAGE = 3,
    
};
typedef NSInteger MessageType;
+(NSString *)messageTypeStr:(MessageType)msgType;
+(MessageType)messageTypeFromStr:(NSString *)messageTypeStr;

enum {
    MESSAGE_STATUS_UNKNOWN, // for recieved message or unknown 0
    MESSAGE_STATUS_WAITING, // local waiting or queued 1
    MESSAGE_STATUS_SERVER, // sent to server 2
    MESSAGE_STATUS_USER,   // recieved by user 3
    MESSAGE_STATUS_READ,   // read by user 4
    MESSAGE_STATUS_FAILED  // failed to send 5
};
typedef NSInteger MessageStatus;


extern NSString *const MESSAGE_RECIEVED_NOTIFICATION;


+(NSString *)getExtensionFromType:(MessageType)msgType;
+(NSString *)getFiletype:(MessageType)msgType;


#define UPDATE_CHAT_LIST @"UPDATE_CHAT_LIST"
@end
