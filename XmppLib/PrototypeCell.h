//
//  PrototypeCell.h
//  XmppLib
//
//  Created by Babul Prabhakar on 03/05/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrototypeCell : UITableViewCell
@property (nonatomic,strong)UILabel *titleLbl;

@end
