//
//  ViewController.h
//  XmppLib
//
//  Created by Babul Prabhakar on 18/04/15.
//  Copyright (c) 2015 Babul Prabhakar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JSQMessagesViewController/JSQMessages.h>
@class Chat;
@interface ViewController : JSQMessagesViewController {
    
}
@property (nonatomic,strong)NSMutableArray *messageArr;
@property (nonatomic,strong)Chat *currentChat;
@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;

@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;
@end

