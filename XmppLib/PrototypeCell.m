//
//  PrototypeCell.m
//  XmppLib
//
//  Created by Babul Prabhakar on 03/05/15.
//  Copyright (c) 2015 craterzone. All rights reserved.
//

#import "PrototypeCell.h"

@implementation PrototypeCell
static int _title_lbl_tag = -1;

@synthesize titleLbl = _titleLbl;

- (void)awakeFromNib {
    // Initialization code
    _titleLbl = (UILabel *)[self viewWithTag:_title_lbl_tag];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
