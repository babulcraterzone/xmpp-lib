#import "XMPPMessageArchiving_Contact_CoreDataObject.h"
#import "Constants.h"

@interface XMPPMessageArchiving_Contact_CoreDataObject ()

@property (nonatomic, strong) XMPPJID * primitiveBareJid;
@property (nonatomic, strong) NSString * primitiveBareJidStr;

@end


@implementation XMPPMessageArchiving_Contact_CoreDataObject

@dynamic bareJid, primitiveBareJid;
@dynamic bareJidStr, primitiveBareJidStr;
@dynamic mostRecentMessageTimestamp;
@dynamic mostRecentMessageBody;
@dynamic mostRecentMessageOutgoing;
@dynamic streamBareJidStr;


@synthesize message;
@synthesize  hresurl;
@synthesize lresurl;
@synthesize messageType;
@synthesize messageState;
@synthesize id;
#pragma mark Transient bareJid

- (XMPPJID *)bareJid
{
	// Create and cache on demand
	
	[self willAccessValueForKey:@"bareJid"];
	XMPPJID *tmp = self.primitiveBareJid;
	[self didAccessValueForKey:@"bareJid"];
	
	if (tmp == nil)
	{
		NSString *bareJidStr = self.bareJidStr;
		if (bareJidStr)
		{
			tmp = [XMPPJID jidWithString:bareJidStr];
			self.primitiveBareJid = tmp;
		}
	}
	
	return tmp;
}

- (void)setBareJid:(XMPPJID *)bareJid
{
	if ([self.bareJid isEqualToJID:bareJid options:XMPPJIDCompareBare])
	{
		return; // No change
	}
	
	[self willChangeValueForKey:@"bareJid"];
	[self willChangeValueForKey:@"bareJidStr"];
	
	self.primitiveBareJid = [bareJid bareJID];
	self.primitiveBareJidStr = [bareJid bare];
	
	[self didChangeValueForKey:@"bareJid"];
	[self didChangeValueForKey:@"bareJidStr"];
}

- (void)setBareJidStr:(NSString *)bareJidStr
{
	if ([self.bareJidStr isEqualToString:bareJidStr])
	{
		return; // No change
	}
	
	[self willChangeValueForKey:@"bareJid"];
	[self willChangeValueForKey:@"bareJidStr"];
	
	XMPPJID *bareJid = [[XMPPJID jidWithString:bareJidStr] bareJID];
	
	self.primitiveBareJid = bareJid;
	self.primitiveBareJidStr = [bareJid bare];
	
	[self didChangeValueForKey:@"bareJid"];
	[self didChangeValueForKey:@"bareJidStr"];
}

#pragma mark Hooks

- (void)willInsertObject
{
    _START
    NSXMLElement *properties = [self.message elementForName:KTAG_PROPERTIES];
    if (properties)
    {
        NSArray *propertyArray = [properties elementsForName:KTAG_PROPERTY];
        self.hresurl = @"";
        self.lresurl = @"";
        self.id = [self.message elementID];
        
        for ( NSXMLElement *property in propertyArray) {
            NSString *name = [[property elementForName:@"name"] stringValue];
            NSString *value = [[property elementForName:@"value"] stringValue];
            if( [name isEqualToString:HRES_URL]  )
            {
                self.hresurl = value;
            }
            else if( [name isEqualToString:LRES_URL]  )
            {
                self.lresurl = value;
            }
            else if( [name isEqualToString:MESSAGE_STATE]  )
            {
                self.messageState = [value intValue];
            }
            else if ([name isEqualToString:MESSAGE_TYPE])
            {
                self.messageType = [value intValue];
            }
        }
    }
    _END
	// If you extend XMPPMessageArchiving_Contact_CoreDataObject,
	// you can override this method to use as a hook to set your own custom properties.
}

- (void)didUpdateObject
{
	// If you extend XMPPMessageArchiving_Contact_CoreDataObject,
	// you can override this method to use as a hook to update your own custom properties.
    [self willInsertObject];
}

@end
